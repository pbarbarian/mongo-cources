Homework 6.5

# 1 Execute
sudo mkdir -p /data/rs1 /data/rs2 /data/rs3

# 2 Then if your user is barbarian, so grant your privileges for these folders
# http://stackoverflow.com/questions/15963147/install-mongodb-child-process-failed-exited-with-error-number-100?rq=1
sudo chown barbarian /data/rs1
sudo chown barbarian /data/rs2
sudo chown barbarian /data/rs3

# 3 Then connect to mongo server
mongo --port 27017

config = { _id: "m101", members:[{ _id : 0, host : "localhost:27017"},{ _id : 1, host : "localhost:27018"},{ _id : 2, host : "localhost:27019"} ]};

