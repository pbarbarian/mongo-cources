#Exam 1
> db.messages.find({"headers.From":"andrew.fastow@enron.com","headers.To":"jeff.skilling@enron.com"}).count()
>: 3

#Exam 2
#see https://jira.mongodb.org/browse/SERVER-13002
db.messages.runCommand("aggregate",{pipeline:[{
	$project: {
		from: "$headers.From",
		to: "$headers.To"
	}
},
{
	$unwind: "$to"
},
{
	$group : { _id : { _id: "$_id", from: "$from", to: "$to" }}
},
{
	$group : { _id : { from: "$_id.from", to: "$_id.to" }, count: {$sum :1}}
},
{
	$sort : {count:-1}
},
{
	$limit: 2
}],allowDiskUse:true})

>: 1 susan.mara@enron.com -> jeff.dasovich@enron.com

# Exam 3
db.messages.update({"headers.Message-ID":"<8147308.1075851042335.JavaMail.evans@thyme>"},{$push:{"headers.To":"mrpotatohead@mongodb.com"}})
Answer is
MongoDB shell version: 3.0.9
connecting to: test
Welcome to the Final Exam Q3 Checker. My job is to make sure you correctly updated the document
Final Exam Q3 Validated successfully!
Your validation code is: vOnRg05kwcqyEFSve96R

# Exam 4
Add the following line to BlogPostDAO.java file where XXX is situated
postsCollection.updateOne(new BasicDBObject("permalink", permalink), new BasicDBObject("$inc", new BasicDBObject("comments." + ordinal + ".num_likes", 1)));

# Exam.question 5
http://joxi.ru/vAW3l3BIkxggQA
(OK) a_1_c_1
_id_
(OK) a_1_b_1_c_-1
(OK) c_1
(OK) a_1_b_1

# Exam.question 6
Add an index on last_name, first_name if one does not already exist.
(OK) Remove all indexes from the collection, leaving only the index on _id in place
Provide a hint to MongoDB that it should not use an index for the inserts
(OK) Set w=0, j=0 on writes
Build a replica set and insert data into the secondary nodes to free up the primary nodes.

# Exam.question 7
# count with
db.albums.aggregate({$unwind:"$images"},{$group:{_id:null,sum:{$sum:"$images"},count:{$sum:1}}})
# count with tag "sunrises"
db.images.find({"tags":"sunrises"}).count()
answer: 44787

# Exam.question 8
# error in ObjectId will be thrown, so only one document is inserted
answer: 1

# Exam.question 9
patient_id - is most logic

# Exam.question 10
The query used an index to figure out which documents match the find criteria.
(OK) The query scanned every document in the collection.
(OK) The query avoided sorting the documents because it was able to use an index's ordering.
The query returned 120,477 documents.