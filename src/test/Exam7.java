import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by barbarian on 5/3/16.
 */
public class Exam7 {
    static final MongoClient mongoClient = new MongoClient(new MongoClientURI("mongodb://localhost"));
    static final MongoDatabase examDatabase = mongoClient.getDatabase("exam");
    static MongoCollection<Document> albumsCollection = examDatabase.getCollection("albums");
    static MongoCollection<Document> imagesCollection = examDatabase.getCollection("images");

    public static void main(String[] args) {
//        List<Document> albums = albumsCollection.find().into(new ArrayList<>());
        List<Document> images = imagesCollection.find().into(new ArrayList<Document>());
        int count = images.size();
        int deleted = 0;


        for (Document doc : images) {
            Object id = doc.get("_id");
            Document album = albumsCollection.find(new BasicDBObject("images", id)).first();
            if (album == null) {
                imagesCollection.deleteOne(new BasicDBObject("_id", id));
                deleted++;
                System.out.println("id : " + id + " / " + count + " | deleted : " + deleted);
            }
        }
        System.out.println("deleted : " + deleted);
    }


}
